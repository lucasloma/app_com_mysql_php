<?php

// Importing DBConfig.php file.
include 'confiDB.php';
 
// Creating connection.
$con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);

$message = '';


if ($con->connect_error)
{
 die("Connection failed: " . $con->connect_error);
} 

 $json = json_decode(file_get_contents('php://input'), true);

$query = "UPDATE carros SET 
marca = '$json[marca]', 
modelo = '$json[modelo]',
ano = '$json[ano]'
WHERE id= '$json[id]'";

$query_result = $con->query($query);

if ($query_result === true)
{
 $message = 'Carro Editado';
}
 
else
{
 $message = 'Error! Tente Novamente';
}
 
echo json_encode($message);