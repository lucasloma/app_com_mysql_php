<?php

// Importing DBConfig.php file.
include 'confiDB.php';
 
// Creating connection.
$con = mysqli_connect($HostName,$HostUser,$HostPass,$DatabaseName);

$message = '';


if ($con->connect_error)
{
 die("Connection failed: " . $con->connect_error);
} 

$json = json_decode(file_get_contents('php://input'), true);

$query = "INSERT INTO carros(marca, modelo ,ano) values('$json[marca]', '$json[modelo]','$json[ano]')";
$query_result = $con->query($query);

if ($query_result === true)
{
 $message = 'Novo Carro Salvo';
}
 
else
{
 $message = 'Error! Tente Novamente';
}
 
echo json_encode($message);
