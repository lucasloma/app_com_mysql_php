

export const LISTA_CARROS_ERROR = 'LISTA_CARROS_ERROR'
export const LISTA_CARROS_SUCCESS = 'LISTA_CARROS_SUCCESS';
export const carrosFetch = () => {
    return dispatch => {
        fetch('http://192.168.0.150:80/app_com_php_mysql/api/verCarro.php')
            .then((response) => (response.json()))
            .then((responseJson) => {
                dispatch({ type: LISTA_CARROS_SUCCESS, payload: responseJson })
            }).catch((error) => {
                dispatch({ type: LISTA_CARROS_ERROR, payload: error.message })
            });

    }
}

export const SET_FIELD = 'SET_FIELD'
export const setField = (field, value) => ({
    type: SET_FIELD,
    field,
    value
});

export const SET_WHOLE_CARRO = 'SET_WHOLE_CARRO';
export const setWholeCarro = (carro) => ({
    type: SET_WHOLE_CARRO,
    payload: carro
});

export const RESET_FORM = 'RESET_FORM';
export const resetForm = () => ({
    type: 'RESET_FORM',
});

export const ADD_CARRO_SUCESSO = 'ADD_CARRO_SUCESSO';
export const saveData = (carros, navigation) => {
    return dispatch => {
        fetch('http://192.168.0.150:80/app_com_php_mysql/api/createCarro.php',
            {
                method: 'POST',
                headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(
                    {
                        modelo: carros.modelo,
                        marca: carros.marca,
                        ano: carros.ano
                    })
            }).then(response => response.json())
            .then(responseJson => {
                alert(responseJson);
                dispatch({ type: ADD_CARRO_SUCESSO });
                dispatch(carrosFetch());
                navigation.navigate('listCarros');
            })
    }
}

export const DEL_CARRO_SUCESSO = 'DEL_CARRO_SUCESSO';
export const deleteDate = (idCarro) => {
    return dispatch => {
        fetch('http://192.168.0.150:80/app_com_php_mysql/api/deleteCarro.php',
            {
                method: 'POST',
                headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(
                    {
                        idCarro: idCarro,
                    })
            }).then(response => response.json())
            .then(responseJson => {
                alert(responseJson);
                console.log(responseJson);
                ///  dispatch({ type: DEL_CARRO_SUCESSO}) ver como utilizar para mensagem de deletatado 
                dispatch(carrosFetch());
            })
    }
}

export const UPDATE_CARRO_SUCESSO = 'UPDATE_CARRO_SUCESSO';
export const updateData = (carro, navigation) => {
    return dispatch => {
        fetch('http://192.168.0.150:80/app_com_php_mysql/api/updateCarro.php',
            {
                method: 'POST',
                headers:
                {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(
                    {
                        id: carro.id,
                        modelo: carro.modelo,
                        marca: carro.marca,
                        ano: carro.ano
                    })
            }).then(response => response.json())
            .then(responseJson => {
                alert(responseJson);
                dispatch({ type: UPDATE_CARRO_SUCESSO });
                dispatch(carrosFetch());
                navigation.navigate('listCarros');
            })
    }
}