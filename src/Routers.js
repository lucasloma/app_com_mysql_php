import { createStackNavigator, createMaterialTopTabNavigator } from 'react-navigation';
import ViewCarros from './pages/viewcarros'
import CadastroFormCarro from './pages/formCarrosPage'


const App = createStackNavigator({
    'listCarros':{
        screen: ViewCarros
    },
    'formCarro':{
        screen: CadastroFormCarro
    }
},{
    navigationOptions:{
        title: 'Carros Mysql',
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: '#3daced',
            borderBottomWidth: 1,
            borderBottomColor: '#c6cece',

        },

        headerTitleStyle: {
            color: 'white',
            fontSize: 30,
            textAlign: "center", /*texto no meio*/
            flexGrow: 1,
        },
    }
});


export default App;