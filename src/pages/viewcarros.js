import React from 'react';
import {
    View,
    Text,
    ListView,
    Button,
    ScrollView,
    Platform,
    StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import { carrosFetch, deleteDate } from '../ations/listCarros';
import _ from 'lodash';

class ViewCarros extends React.Component {
    componentWillMount() {
        this.props.carrosFetch();
        this.criaFonteDados(this.props.carros)
    }

    ///este método e executado depois da montagem no componente ou quando alterado uma props
    componentWillReceiveProps(nextProps) {
        this.criaFonteDados(nextProps.carros);
    }

    criaFonteDados(carros) {
        ///Preparação do dataSource do LisView onde vai tratar os dados nas linhas 
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
        this.fonteDados = ds.cloneWithRows(carros)
    }

    _deleteCarro(idCarro) {
        this.props.deleteDate(idCarro);
    }

    _updateCarro(carro) {
        this.props.navigation.navigate('formCarro', { carroUpdate: carro })
    }

    renderRow(carro) {
        return (
            <View style={styles.ViewCarros}>
                <View>
                    <Text style={{ fontSize: 25 }}> Modelo : {carro.modelo}</Text>
                    <Text style={{ fontSize: 20 }}> Marca : {carro.marca}</Text>
                    <Text style={{ fontSize: 20 }}> Ano : {carro.ano}</Text>
                </View>
                <View style={styles.botoesCrud}>
                    <Button
                        title='Deletar'
                        color='red'
                        onPress={() => this._deleteCarro(carro.id)} />
                    <Button
                        title='Editar'
                        color='green'
                        onPress={() => this._updateCarro(carro)} />
                </View>
            </View>
        );
    }

    render() {
        const OS = Platform.OS === 'ios' ? 'Estou no IOS' : 'Estou no Andriod';
        const Version = Platform.Version;
        return (
            <View style={styles.container}>
                <Text style={{ backgroundColor: '#C6E9F8' }}>{`${OS} versão ${Version}`}</Text>
                <ScrollView style={styles.lstCarros}>
                    <ListView
                        enableEmptySections /// para para mensagem de erro do header
                        dataSource={this.fonteDados}
                        renderRow={data => this.renderRow(data)}
                    />
                </ScrollView>
                <View style={styles.botaoNovoCarro}>
                    <Button
                        title='Novo Carro'
                        onPress={() => this.props.navigation.navigate('formCarro')}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    lstCarros: {
        flex: 4
    },
    ViewCarros: {
        flex: 1,
        flexDirection: 'row',
        padding: 5,
        margin: 5,
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderColor: '#CCC'
    },
    botaoNovoCarro: {
        margin: 20
    },
    botoesCrud: {
        justifyContent: 'space-between',
    }
})

const mapStateProps = (state) => {
    const carros = _.map(state.ListCarros, (val, uid) => {
        return { ...val, uid }
    });
    return {
        carros
    }
}

const mapDispatchProps = {
    carrosFetch,
    deleteDate
}

export default connect(mapStateProps, mapDispatchProps)(ViewCarros);