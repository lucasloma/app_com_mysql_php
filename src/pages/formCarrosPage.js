import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    ActivityIndicator,
    Platform,
    ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import { setField, saveData, setWholeCarro, resetForm, updateData } from '../ations/listCarros'

class CadastroFormCarro extends React.Component {
    componentDidMount() {
        const { navigation, setWholeCarro, resetForm } = this.props;
        const { params } = navigation.state;
        if (params && params.carroUpdate) {
            return setWholeCarro(params.carroUpdate)
        }
        return resetForm();
    }

    render() {
        const { formCarrosReducers } = this.props
        return (
            <ScrollView style={{ flex: 1 }}>
                <View style={styles.container}>
                    <View style={styles.containerTexto}>
                        <Text style={styles.titulo}>
                            {formCarrosReducers.id ? `Editar  ${formCarrosReducers.modelo}` : 'Cadastrar'}
                        </Text>
                    </View>
                    <View style={styles.containerInput}>
                        <TextInput value={formCarrosReducers.modelo}
                            style={styles.inputTexto} placeholder='Modelo'
                            onChangeText={texto => this.props.setField('modelo', texto)} />
                        <TextInput value={formCarrosReducers.marca}
                            style={styles.inputTexto} placeholder='Marca'
                            onChangeText={texto => this.props.setField('marca', texto)} />
                        <TextInput value={formCarrosReducers.ano}
                            style={styles.inputTexto} placeholder='ANO'
                            keyboardType={Platform.OS === 'ios' ? 'numbers-and-punctuation' : 'numeric'}   ///habilta tipo teclado para cada os
                            onChangeText={texto => this.props.setField('ano', texto)} />
                        <Text style={styles.textoErro}>{formCarrosReducers.mensagemErro}</Text>
                    </View>
                    <View style={styles.containerBotao}>
                        {this.renderButton()}
                    </View>
                </View>
            </ScrollView>
        );
    }

    renderButton() {
        const { isLonding, id } = this.props.formCarrosReducers;
        if (isLonding) {
            return <ActivityIndicator size='large' />
        }
        return (
            <Button title={id ? 'Editar' : 'Cadastrar'}
                color="#115E54"
                onPress={() => (
                    id ? this._updateCarro() : this._cadastrarCarro()
                )} />
        );
    }

    _cadastrarCarro() {
        this.props.saveData(this.props.formCarrosReducers, this.props.navigation)
    }

    _updateCarro() {
        this.props.updateData(this.props.formCarrosReducers, this.props.navigation)
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        backgroundColor: '#FFF'
    },
    containerTexto: {
        flex: 1,
        justifyContent: 'center',
        alignItems: "center"
    },
    titulo: {
        fontSize: 25,
        color: '#000',
        fontWeight: 'bold',
    },
    containerInput: {
        flex: 2,
    },
    inputTexto: {
        fontSize: 20,
        height: 45,
    },
    containerBotao: {
        flex: 2
    },
    textoErro: {
        color: 'red',
        fontSize: 20
    }
});

mapStateToProps = state => {
    return {
        formCarrosReducers: state.formCarrosReducers
    }
}

const mapDispatchToProps = {
    setField,
    saveData,
    setWholeCarro,
    resetForm,
    updateData
};

export default connect(mapStateToProps, mapDispatchToProps)(CadastroFormCarro);