import {SET_FIELD,ADD_CARRO_SUCESSO,SET_WHOLE_CARRO,RESET_FORM,UPDATE_CARRO_SUCESSO} from '../ations/listCarros'

const INITIAL_STATE = {
    id: '',
    modelo: '',
    marca: '',
    ano: '',
    mensagemErro: '',
    isLonding: false /// por enquanto
}

export default function formCarrosReducers(state = INITIAL_STATE, action) {
    switch (action.type) {
        case SET_FIELD:
            const newSatate = { ...state };
            newSatate[action.field] = action.value;
            return newSatate;
        case SET_WHOLE_CARRO:
            return action.payload
        case ADD_CARRO_SUCESSO:
            return INITIAL_STATE;
        case RESET_FORM:
            return INITIAL_STATE;
        case UPDATE_CARRO_SUCESSO:
            return INITIAL_STATE;
        default:
            return state;
    }
}