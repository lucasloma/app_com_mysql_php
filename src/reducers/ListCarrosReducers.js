import { LISTA_CARROS_SUCCESS, DEL_CARRO_SUCESSO } from '../ations/listCarros'

INITIAL_STATE = {};

export default function ListCarros(state = INITIAL_STATE, action) {
    switch (action.type) {
        case LISTA_CARROS_SUCCESS:
            return action.payload;
        case DEL_CARRO_SUCESSO:
            return action.payload;
        default:
            return state;

    }
}