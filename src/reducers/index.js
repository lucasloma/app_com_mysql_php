import {combineReducers} from 'redux';
import ListCarros from './ListCarrosReducers';
import formCarrosReducers from './formCarrosReducers'

export default combineReducers({
    ListCarros,
    formCarrosReducers
});